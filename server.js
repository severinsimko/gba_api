var express = require('express');
var app = express();
const logger = require('morgan')
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: "false" }));
app.use(bodyParser.json());


async function querydatabase(first_name,last_name,address,zipcode){
    const { Client } = require('pg')
    const client = new Client({
        user: 'GBA DB USER',
        host: 'GBA_DB HOSTNAME',
        database: 'GBA DB DATABASE',
        password: 'PASSWORD',
        port: "GBA DB PORT",
    })

   return new Promise( (resolve, reject) => {

    try{
        client.connect()
        query = "select lookupconsumer('" + first_name + "','" + last_name + "','" + address + "','" + zipcode + "','');"
        res2 = client.query(query, (err, res) => {

            if (res === 'undefined' || res.rowCount === 'undefined'){
                error_message = '{"success":"false", "message":"unknown"}'
                client.end()
                resolve(error_message)
            }else{
                if (res.rowCount === 0){
                    error_message = '{"success":"true", "message":"unknown"}'
                    client.end()
                    resolve(error_message)
                }else if (res.rowCount === 1){
                    error_message = '{"success":"true", "message":\"' + res.rows[0]["lookupconsumer"]["lender_segment"] + '\"}'
                    client.end()
                    resolve(error_message)
                }else{
                    error_message = '{"success":"false", "message":"unknown"}'
                    client.end()
                    resolve(error_message)
                }
            }
       })
    }catch(error){
        error_message = '{"success":"false", "message":"unknown"}'
        reject(error_message)
        }
      });
}

app.use(logger(':method :url :status :res[content-length] - :response-time ms'))


app.post('/get_score_post', async function (req, res) {

    var token = req.headers['x-access-token'];
    if (!token){
        error_message = '{"success":"false", "message":"Missing Authentication Token"}'
        res.end(error_message);
    }
    var token_auth = "TOKEN"

    if (token !== token_auth){
	error_message = '{"success":"false", "message":"Incorrect Authentication Token"}'
        res.end(error_message);
    }

    try{
        first_name = req.body.first_name.toUpperCase();
        last_name = req.body.last_name.toUpperCase();
        address = req.body.address.toUpperCase();
        zipcode = req.body.zipcode.toUpperCase();

        //Remove %20 from the URL white space
        first_name = first_name.replace(/%20/g, " ");
        last_name = last_name.replace(/%20/g, " ");
        address = address.replace(/%20/g, " ");
        zipcode = zipcode.replace(/%20/g, " ");

        first_name = first_name.replace(/'/g, "''");
        last_name = last_name.replace(/'/g, "''");
        address = address.replace(/'/g, "''");
        zipcode = zipcode.replace(/'/g, "''");

        first_name = first_name.replace(/"/g, "''");
        last_name = last_name.replace(/"/g, "''");
        address = address.replace(/"/g, "''");
        zipcode = zipcode.replace(/"/g, "''");
    }catch(error){
        error_message = '{"success":"false", "message":"unknown"}'
        res.end(error_message);
    }
    console.log("Processing: " + first_name + " " + last_name + " " + address + " " + zipcode)
    if ( first_name === '' || first_name === null  || last_name === '' || last_name === null || address === '' || address === null || zipcode === '' || zipcode === null ){
        error_message = '{"success":"false", "message":"Missing parameter"}'
        res.end(error_message);
    }else if(address.length > 30 || first_name.length > 48 || last_name.length > 48 || zipcode.length > 48 ){
        error_message = '{"success":"false", "message":"Params are too long - lead skipped"}'
        res.end(error_message);
    }else{
       query_return = await querydatabase(first_name,last_name,address,zipcode)
        res.end(query_return);
    }
});

app.use(function (req, res) {
    error_message = '{"success":"false", "message":"Missing parameter"}'
    res.status(404).send(error_message)
})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})

//Request example
//http://34.212.93.86:8081/get_score/first_name=MARK/last_name=SPLITTGERBER/address=5705%20ASPEN%20DR/zipcode=50266
